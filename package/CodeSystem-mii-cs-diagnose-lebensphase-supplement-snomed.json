{
  "resourceType": "CodeSystem",
  "id": "mii-cs-diagnose-lebensphase-supplement-snomed-v2025.0.x",
  "meta": {
    "profile": [
      "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
    ],
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "cds",
        "display": "Core Dataset of the MII"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "cds-diagnose",
        "display": "CDS Module 'Diagnose'"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#mii-cds",
        "display": "MII CDS license"
      }
    ]
  },
  "extension": [
    {
      "url": "https://www.medizininformatik-initiative.de/fhir/modul-meta/StructureDefinition/mii-ex-meta-license-codeable",
      "valueCodeableConcept": {
        "coding": [
          {
            "system": "http://hl7.org/fhir/spdx-license",
            "code": "CC-BY-4.0",
            "display": "Creative Commons Attribution 4.0 International"
          }
        ]
      }
    },
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/de.medizininformatikinitiative.kerndatensatz.terminology.diagnose"
    }
  ],
  "url": "https://www.medizininformatik-initiative.de/fhir/core/modul-diagnose/CodeSystem/mii-cs-diagnose-lebensphase-supplement-snomed",
  "version": "2025.0.0",
  "name": "MII_CS_Diagnose_Lebensphase_Supplement_SNOMED",
  "status": "active",
  "experimental": true,
  "date": "2024-11-13",
  "publisher": "Medizininformatik Initiative",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "https://www.medizininformatik-initiative.de"
        }
      ]
    }
  ],
  "description": "CodeSystem Supplement mit Definitionen zu den SNOMED-Konzepten für Lebensphasen.",
  "copyright": "This material includes SNOMED Clinical Terms® (SNOMED CT®) which is used by permission of SNOMED International. All rights reserved. SNOMED CT®, was originally created by The College of American Pathologists. SNOMED and SNOMED CT are registered trademarks of SNOMED International. Implementers of these artefacts must have the appropriate SNOMED CT Affiliate license.",
  "content": "supplement",
  "supplements": "http://snomed.info/sct",
  "property": [
    {
      "code": "comment",
      "uri": "http://hl7.org/fhir/concept-properties#comment",
      "type": "string"
    }
  ],
  "concept": [
    {
      "code": "271872005",
      "display": "Old age (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Ältere Person/ Senioren (mit Beginn des 65. Lebensjahres)"
        }
      ]
    },
    {
      "code": "41847000",
      "display": "Adulthood (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Erwachsener (ab Beginn des 19. Lebensjahres)"
        }
      ]
    },
    {
      "code": "263659003",
      "display": "Adolescence (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Jugendlicher (ab Beginn des 13. bis zum vollendeten 18. Lebensjahres)"
        }
      ]
    },
    {
      "code": "255398004",
      "display": "Childhood (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Kind (ab Beginn des 4. bis zum vollendeten 12. Lebensjahres)"
        }
      ]
    },
    {
      "code": "713153009",
      "display": "Toddler (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Kleinkind (ab Beginn des 13. Lebensmonat bis zum vollendeten 3. Lebensjahr)"
        }
      ]
    },
    {
      "code": "3658006",
      "display": "Infancy (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Säugling (ab Beginn des 29. Lebenstages bis zum vollendeten 12. Lebensmonat)"
        }
      ]
    },
    {
      "code": "255407002",
      "display": "Neonatal (qualifier value)",
      "property": [
        {
          "code": "comment",
          "valueString": "Neugeborenes (bis zum vollendeten 28. Lebenstag)"
        }
      ]
    }
  ],
  "title": "MII_CS_Diagnose_Lebensphase_Supplement_SNOMED",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>MII_CS_Diagnose_Lebensphase_Supplement_SNOMED</h2><tt>https://www.medizininformatik-initiative.de/fhir/core/modul-diagnose/CodeSystem/mii-cs-diagnose-lebensphase-supplement-snomed</tt><p>CodeSystem Supplement mit Definitionen zu den SNOMED-Konzepten für Lebensphasen.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>Core Dataset of the MII</td></tr><tr><td><b>Dataset</b></td><td>CDS Module &#x27;Diagnose&#x27;</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#mii-cds\">MII CDS license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/de.medizininformatikinitiative.kerndatensatz.terminology.diagnose\"><code>@mii-termserv/de.medizininformatikinitiative.kerndatensatz.terminology.diagnose</code></a></td></tr></tbody></table></div></div>"
  }
}
## Terminologie MII KDS Basismodul Diagnose

Dieses Paket enthält die Terminologie aus dem Basismodul Diagnose des Kerndatensatzes der MII.

Diese Ressourcen wurden umpaketiert aus dem Ursprungspaket: https://simplifier.net/packages/de.medizininformatikinitiative.kerndatensatz.diagnose/2025.0.0.

---

Sobald neue Versionen veröffentlicht werden, werden diese auf anderen Git-Branches verfügbar gemacht. Sie sehen aktuell den Stand vom Branch **2025**.

Weitere Branches:
- [2025](https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.diagnose/-/tree/2025)

#!/bin/bash

files=$(find ./package -name "*.json" -not -name "package.json" -not -name ".index.json")
cat /dev/null >| package/.index.ndjson.tmp
for filename in $files; do
  echo "adding $filename to index"
  resource_type=$(jq -rc '.resourceType' $filename)
  id=$(jq -rc '.id' $filename)
  url=$(jq -rc '.url' $filename)
  version=$(jq -rc '.version' $filename)

  json_string=$(jq -nc --arg file_name "$filename" --arg resource_type "$resource_type" --arg id "$id" --arg url "$url" --arg version "$version" '{filename: $file_name, resourceType: $resource_type, id: $id, url: $url, version: $version}')
  echo "$json_string" >> package/.index.ndjson.tmp
done

jq -c --slurp '.' package/.index.ndjson.tmp | jq '{"index-version": 1, "files": .}' > package/.index.json
rm package/.index.ndjson.tmp